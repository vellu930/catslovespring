package com.example.springboot;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties("cat")
public class ModelCat {
    private String catName;
    private String catType;
    private int kittens;
    private String[] kittensName;
}

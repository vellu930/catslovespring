package com.example.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:cats.properties")
public class Config {

    @Autowired
     private Environment env;

    @Bean
    public ModelCat modelCat() {
        ModelCat catEnv = new ModelCat();
        catEnv.setCatName(env.getProperty("bean.name"));
        return catEnv;
    }
}

package com.example.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @Autowired
    private ModelCat modelCat;

    @GetMapping("/cats")
    public String index() {
        return "Greetings from Cat's Shelter Skelter! " +modelCat.getCatName();
    }

}
